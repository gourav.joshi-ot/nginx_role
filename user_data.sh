#!/bin/bash
REPO_URL=https://gitlab.com/gourav.joshi-ot/nginx_role.git
yum install git -y
pip install ansible
git clone $REPO_URL
cd nginx_role
/usr/local/bin/ansible-playbook nginx_role.yml