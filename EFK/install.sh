#!/bin/bash
REPO_URL=https://gitlab.com/gourav.joshi-ot/nginx_role.git
apt-get install git -y
apt-add-repository ppa:ansible/ansible
apt update -y
apt-get install ansible -y 
git clone $REPO_URL
cd nginx_role/EFK
ansible-playbook main.yml
